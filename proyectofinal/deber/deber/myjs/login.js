var usuariosRegistrados = [];
// json lo parsea a array 
// && es una compuerta an para que las dos se cumplan si se pone or solo con una ya se puede entrar
function ingresarSistema(usuario, clave, sesionIniciada = false) {
    console.log(usuario);
    for (let i = 0; i < usuariosRegistrados.length; i++) {
        // este if es para que cumplas las condiciones al momento de registrarse
      if (
        usuario == usuariosRegistrados[i].usuario &&
        clave == usuariosRegistrados[i].clave
      ) {
          //si el usuario esta registrado entonces la sesion estara iniciada
        usuariosRegistrados[i].sesionIniciada = sesionIniciada;
        localStorage.setItem(
          "usuarioLogueado",
          JSON.stringify(usuariosRegistrados[i])
        );
        redireccionar();
        return;
      }
    }
    Swal.fire({
      icon: 'error',
      title: 'Verifique sus datos ',
      text: 'Usuario y/o contraseña no son válidos',
      footer: '<a verifique sus datos?</a>'
    })
    return;
  }
  
  function verificarInicioSesion() {
    let usuarioLogueado = JSON.parse(localStorage.getItem("usuarioLogueado"));
    if (usuarioLogueado != null && usuarioLogueado.sesionIniciada == true) {
      
      Swal.fire({
        title: 'Ya tiene una sesion Iniciada ',
        text: 'Redireccionando'
      })
      setTimeout(redireccionar, 3000);
    }
  }
  
  function redireccionar() {
    window.location = "../index.html";
  }
  
  function obtenerUsuarios() {
    usuariosRegistrados = JSON.parse(localStorage.getItem("usuariosRegistrados"));
    if (usuariosRegistrados == null  || usuariosRegistrados.length == 0 ) {
      usuariosRegistrados = [
        {
          usuario: "admin",
          clave: "123",
          nombre: "juan",
          apellido: "perez",
          sesionIniciada: false,
        },
      ];
    }
  }

  obtenerUsuarios();
  verificarInicioSesion();

// que funcion cumple el locarStore

// que diferencia hay en que el lenguaje este de español al ingles 

// JSON.parse si se puede parsear todo el codigo en vez de una a la vez .

// que funcion cumple la linea 28 crearUsuariosTemporales.

// en la function ingresarSistema que funcion realiza el false en sesion iniciada
// que funcion cumple el null en la function
// porque va el redireccionar de la function ingresarSistema de la linea 19 antes del return y no en otra linea de codigo
//para que sirve el setTimeOut