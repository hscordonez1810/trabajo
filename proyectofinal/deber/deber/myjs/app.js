function cargarDatosUsuario() {
  document.getElementById("nombreUsuario").innerHTML = usuarioLogueado.usuario;
}

function cerrarSesion() {
  localStorage.removeItem("usuarioLogueado");
  redireccionarLogin();
}

// yo ..

function redireccionarLogin() {
  window.location = "page/login.html";
 
}

function verificarInicioSesion() {
  
  if (usuarioLogueado == null) {
    Swal.fire({
      icon: 'error',
      title: 'No ha iniciado sesiòn ',
      text: 'redireccionando al login'
    })
    
    redireccionarLogin();
  } else {
    cargarDatosUsuario();
  }
}

// yo
function crearUsuario(
  usuarioId,
  claveUsuario,
  nombreUsuario,
  apellidoUsuario,
  sesionIniciadaUsuario
) {
  let nuevoUsuario = {
    usuario: usuarioId,
    clave: claveUsuario,
    nombre: nombreUsuario,
    apellido: apellidoUsuario,
    sesionIniciada: sesionIniciadaUsuario,
  };

  if (usuariosRegistrados == null) {
    usuariosRegistrados = [];
  }
  for (let i = 0; i < usuariosRegistrados.length; i++) {
    if (nuevoUsuario.usuario == usuariosRegistrados[i].usuario) {
      Swal.fire({
        icon: 'error',
        title: 'Usuario ya existente ',
        text: 'Intente de Nuevo'
      })
      return;
    }
  }
  usuariosRegistrados.push(nuevoUsuario);

  localStorage.setItem(
    "usuariosRegistrados",
    JSON.stringify(usuariosRegistrados)
  );
  listarUsuarios();
}

function  actualizarUsuario(usuarioId , claveUsuario , nombreUsuario, apellidoUsuario , sesionIniciadaUsuario){
  let actualizar={
    usuario:usuarioId ,
    clave:claveUsuario,
    nombre:nombreUsuario,
    apellido:apellidoUsuario,
  sesionIniciada:sesionIniciadaUsuario 
};
for(let i=0; i<usuariosRegistrados.length;i++){
  if(usuariosRegistrados[i].usuario == usuarioId){
    usuariosRegistrados[i] =actualizar;
    localStorage.setItem("usuariosRegistrados" , JSON.stringify(usuariosRegistrados));
    listarUsuarios();
    return;
}
}
Swal.fire({
  icon: 'error',
  title: 'No existe el usuario ',
  text: 'para actualizar'
})
}

// yo

function listarUsuarios() {
  if(usuariosRegistrados != null){
  document.getElementById("listaUsuarios").innerHTML = "";
  for (let i = 0; i < usuariosRegistrados.length; i++) {

    document.getElementById("listaUsuarios").innerHTML +=
      `<tr>
      <td>
        ${usuariosRegistrados[i].usuario}
      </td> 
      <td>
        ${usuariosRegistrados[i].clave}
      </td> 
      <td>
        ${usuariosRegistrados[i].nombre}
      </td>
      <td>
        ${usuariosRegistrados[i].apellido}
      </td> 
      <td>
        ${usuariosRegistrados[i].sesionIniciada}
      </td> 
    <td>
    <button
    type="button"
    class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop"
    onclick='editarUsuario(${JSON.stringify(usuariosRegistrados[i])})'>
    editar
  </button>

  <button
  type="button"
  class="btn btn-danger"
  onclick="eliminarUsuario('${usuariosRegistrados[i].usuario}')">
 eliminar
</button>
</td>
</tr>
    `;
  }
}
}

function eliminarUsuario(usuario){
 
for(let i=0; i< usuariosRegistrados.length;i++){
  
  if(usuariosRegistrados[i].usuario == usuario){
    usuariosRegistrados.splice(i,1)

  }
    
  } 
 

localStorage.setItem("usuariosRegistrados", JSON.stringify(usuariosRegistrados));
listarUsuarios();
}

// yo
 function editarUsuario(usuario){
   document.getElementById("usuario").value= usuario.usuario;
   document.getElementById("clave").value= usuario.clave;
   document.getElementById("nombre").value= usuario.nombre;
   document.getElementById("apellido").value= usuario.apellido;
   document.getElementById("sesionIniciada").checked= usuario.sesionIniciada;
 }

var usuariosRegistrados = JSON.parse(
  localStorage.getItem("usuariosRegistrados")
);
var usuarioLogueado = JSON.parse(localStorage.getItem("usuarioLogueado"));
verificarInicioSesion();
listarUsuarios();
